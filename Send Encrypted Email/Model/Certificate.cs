﻿using ILG.Events;
using System;
using System.IO;

namespace SendEncryptedEmail.Model
{
    public class Certificate : PropertyChangedHelper
    {
        #region Variables
        private int _Id;
        private CertificateType _CertificateType;
        private string _Name;
        private string _Password;
        private string _Extension;
        private DateTime _ExpiryDate;
        #endregion

        #region Properties
        public int Id 
        { 
            get { return _Id; }
            set
            {
                _Id = value;
                NotifyPropertyChanged(() => Id);
            }
        }   // End Property Id

        public CertificateType CertificateType
        {
            get { return _CertificateType; }
            set
            {
                _CertificateType = value;
                NotifyPropertyChanged(() => CertificateType);
            }
        }   // End Property CertificateType

        public string Name 
        { 
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged(() => Name);
            }
        }   // End Property Name

        public string Extension
        {
            get { return _Extension; }
            set
            {
                _Extension = value;
                NotifyPropertyChanged(() => Extension);
            }
        }   // End Property Extension

        public string Password
        {
            get { return _Password; }
            set
            {
                _Password = value;
                NotifyPropertyChanged(() => Password);
            }
        }   // End Property Password

        public DateTime ExpiryDate 
        { 
            get { return _ExpiryDate; }
            set 
            {
                _ExpiryDate = value;
                NotifyPropertyChanged(() => ExpiryDate);
            }
        }   // End Property ExpiryDate
        #endregion

        #region Methods
        public bool CertificateIsValid()
        {
            if (string.IsNullOrWhiteSpace(_Name))
                return false;

            if (_CertificateType == CertificateType.Signing)
            {
                if (string.IsNullOrWhiteSpace(_Password))
                    return false;
            }

            return true;

        }   // End Property CertificateIsValid

        public string GetFileName()
        {
            string filePath;
            string fileName;

            filePath = Configuration.GetCertificatePath();
            if (filePath == null)
                return null;

            fileName = Path.Combine(filePath, string.Format("{0}.{1}", _Id, _Extension));

            return fileName;

        }   // End Method GetFileName

        public Certificate DeepCopy()
        {
            return (Certificate)MemberwiseClone();
        }   // End Method DeepCopy
        #endregion

    }   // End Class Certificate
}
