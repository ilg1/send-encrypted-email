﻿using ILG.Events;
using SendEncryptedEmail.ViewModel;
using System;
using System.Xml.Serialization;
using EA = EASendMail;

namespace SendEncryptedEmail.Model
{
    public class Contact : PropertyChangedHelper
    {
        #region Variables
        private int _Id;
        private string _Name;
        private string _Email;
        private Certificate _Certificate;
        #endregion

        #region Properties
        public int Id 
        { 
            get { return _Id; }
            set { _Id = value; }
        }   // End Property Id

        public string Name 
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyPropertyChanged(() => Name);
            }
        }   // End Property Name

        public string Email 
        { 
            get { return _Email; }
            set
            {
                _Email = value;
                NotifyPropertyChanged(() => Email);
            }
        }   // End Property Email

        [XmlIgnore]
        public string DisplayName 
        { 
            get { return $"{_Name} ({_Email})"; }
        }   // End Property DisplayName

        public Certificate Certificate
        { 
            get { return _Certificate; }
            set
            {
                _Certificate = value;
                NotifyPropertyChanged(() => Certificate);
            }
        }   // End Property Certificate

        [XmlIgnore]
        public bool HasCertificate
        {
            get
            {
                if (_Certificate == null)
                    return false;
                return true;
            }
        }   // End Property HasCertificate
        #endregion

        #region Methods
        public bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(_Name))
                return false;
            
            if (string.IsNullOrWhiteSpace(_Email))
                return false;
            
            return true;

        }   // End Method IsValid

        public EA.MailAddress ContactToMailAddress(bool addEncryptionCertificate)
        {
            EA.MailAddress mailAddress;

            mailAddress = new EA.MailAddress(_Email);
            if (addEncryptionCertificate && HasCertificate)
            {
                try
                {
                    mailAddress.Certificate.Load(_Certificate.GetFileName());
                }
                catch (Exception ex)
                {
                    SendEncryptedEmailMessageBox.ShowError($"Unable to load Encruption Certificate for {_Name}.\n\n{ex.Message}");
                }
            }

            return mailAddress;

        }   // End Method ContactToMailAddress

        public Contact DeepCopy()
        {
            Contact contact;

            contact = (Contact)MemberwiseClone();
            if (_Certificate != null)
                contact.Certificate = _Certificate.DeepCopy();

            return contact;

        }   // End Method DeepCopy
        #endregion

    }   // End Class Contact

}   // End Namespace SendEncryptedEmail.Model
