﻿using SendEncryptedEmail.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace SendEncryptedEmail.Model
{
    public class AddressBook
    {
        #region Constants
        private const string _AddressBookFileName = "AddressBook.xml";
        #endregion

        #region Variables
        private static AddressBook _Instance = null;
        private ObservableCollection<Contact> _ContactList;
        private int _LastContactId;
        #endregion

        #region Constructor
        private AddressBook()
        {
            _ContactList = new ObservableCollection<Contact>();
            _LastContactId = 0;
        }   // End Constructor AddressBook
        #endregion

        #region Properties
        [XmlIgnore]
        public static AddressBook Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new AddressBook();
                }
                return _Instance;
            }
        }   // End Property Instance

        [XmlArray("ContactList")]
        [XmlArrayItem("Contact")]
        public ObservableCollection<Contact> ContactList 
        { 
            get { return _ContactList; }
            set { _ContactList = value; }
        }   // End Property ContactList
        #endregion

        #region Methods
        public static void AddContact(Contact contact)
        {
            Instance._LastContactId += 1;
            contact.Id = Instance._LastContactId;
            Instance.ContactList.Add(contact);
        }   // End Method AddContact

        public static void RemoveContact(Contact contact)
        {
            Instance.ContactList.Remove(contact);
        }   // End Method RemoveContact

        public static void Initialise()
        {
            _Instance = Load();
            Instance._LastContactId = 0;
            if (Instance.ContactList != null)
            {
                foreach (Contact contact in Instance.ContactList)
                {
                    if (contact.Id > Instance._LastContactId)
                        Instance._LastContactId = contact.Id;
                }
            }
            else
                Instance.ContactList = new ObservableCollection<Contact>();
        }   // End Method Initialise

        public static void Save()
        {
            Save(_Instance);
        }   // End Method Save

        public static void Save(AddressBook addressBook)
        {
            XmlSerializer serializer;
            string filePath;
            string fileName;

            filePath = Configuration.GetConfigurationPath();
            if (string.IsNullOrWhiteSpace(filePath))
                return;

            fileName = Path.Combine(filePath, _AddressBookFileName);

            serializer = new XmlSerializer(typeof(AddressBook));
            try
            {
                using (FileStream stream = new FileStream(fileName, FileMode.Create))
                {
                    using (TextWriter textWriter = new StreamWriter(stream))
                    {
                        serializer.Serialize(textWriter, addressBook);
                    }
                }
            }
            catch (Exception ex)
            {
                SendEncryptedEmailMessageBox.ShowError($"Failed to save Send Encrypted Email Address Book file.\n\n{ex.Message}");
            }

        }   // End Method Save

        public static AddressBook Load()
        {
            XmlSerializer serializer;
            AddressBook addressBook;
            string filePath;
            string fileName;

            filePath = Configuration.GetConfigurationPath();
            if (string.IsNullOrWhiteSpace(filePath))
                return null;

            fileName = Path.Combine(filePath, _AddressBookFileName);

            if (!File.Exists(fileName))
                return null;

            using (FileStream stream = new FileStream(fileName, FileMode.Open))
            {
                serializer = new XmlSerializer(typeof(AddressBook));
                using (TextReader textReader = new StreamReader(stream))
                {
                    try
                    {
                        addressBook = (AddressBook)serializer.Deserialize(textReader);
                    }
                    catch (Exception ex)
                    {
                        SendEncryptedEmailMessageBox.ShowError($"Failed to load Send Encrypted Email Address Book file.\n\n{ex.Message}");
                        return null;
                    }
                }

                return addressBook;
            }
        }   // End Method Load
        #endregion

    }   // End Class AddressBook

}   // End Namespace SendEncryptedEmail.Model
