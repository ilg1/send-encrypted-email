﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SendEncryptedEmail.Model
{
    public struct CertificateType : IXmlSerializable
    {
        private enum CertificateTypeType
        {
            None,
            Signing,
            Encryption
        }   // End Enum CertificateTypeType

        #region Constants
        public const int CertificateTypeTypeLength = 1;
        private readonly CertificateTypeType _CertificateType;
        public static readonly CertificateType None = new CertificateType(CertificateTypeType.None);
        public static readonly CertificateType Signing = new CertificateType(CertificateTypeType.Signing);
        public static readonly CertificateType Encryption = new CertificateType(CertificateTypeType.Encryption);
        public static readonly List<CertificateType> CertificateTypeList = new List<CertificateType>() { Signing, Encryption };
        public static readonly List<CertificateType> CertificateTypeListIncludingNone = new List<CertificateType>() { None, Signing, Encryption };
        #endregion

        #region Constructor
        private CertificateType(CertificateTypeType CertificateTypeType)
        {
            _CertificateType = CertificateTypeType;
        }   // End Constructor CertificateType

        public CertificateType(string CertificateTypeTypeCode)
        {
            _CertificateType = ((CertificateType)CertificateTypeTypeCode)._CertificateType;
        }   // End Constructor  CertificateType
        #endregion

        #region Properties
        public string LongName
        {
            get { return ToLongString(); }
        }   // End Property LongName
        #endregion

        #region Methods
        public static implicit operator string(CertificateType value)
        {
            switch (value._CertificateType)
            {
                case CertificateTypeType.Signing:
                    return "S";
                case CertificateTypeType.Encryption:
                    return "E";
                default:
                case CertificateTypeType.None:
                    return "";
            }
        }   // End Operator String

        public static implicit operator CertificateType(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return new CertificateType(CertificateTypeType.None);

            switch (value)
            {
                case "S":
                    return new CertificateType(CertificateTypeType.Signing);
                case "E":
                    return new CertificateType(CertificateTypeType.Encryption);
            }

            // If we get to here than an invalid code was passed in so throw an exception
            throw new ArgumentOutOfRangeException("value");

        }   // End Operator PriceType

        public string ToShortString()
        {
            return this;
        }   // End Method ToShortString

        public string ToLongString()
        {
            switch (_CertificateType)
            {
                case CertificateTypeType.Signing:
                    return "Signing";
                case CertificateTypeType.Encryption:
                    return "Encryption";
                default:
                case CertificateTypeType.None:
                    return "None";
            }
        }   // End Method ToLongString

        public static bool TryParse(string input, out CertificateType CertificateType)
        {
            // Enum makes it easy to do parsing
            CertificateTypeType value;

            if (Enum.TryParse<CertificateTypeType>(input, out value))
            {
                CertificateType = new CertificateType(value);
                return true;
            }
            else
            {
                CertificateType = default(CertificateType);
                return false;
            }
        }   // End Method TryParse

        public override string ToString()
        {
            return ToShortString();
        }   // End Method ToSrtring

        public override bool Equals(object obj)
        {
            CertificateTypeType value;

            if (obj is CertificateType)
                return this == (CertificateType)obj;
            if (obj is string)
            {
                if (string.IsNullOrWhiteSpace(obj as string))
                    value = CertificateTypeType.None;
                else
                    value = ((CertificateType)(obj as string))._CertificateType;
                return value == _CertificateType;
            }

            return _CertificateType.Equals(obj);

        }   // End Method Equals

        public override int GetHashCode()
        {
            return _CertificateType.GetHashCode();
        }   // End Method GetHashCode

        public XmlSchema GetSchema()
        {
            return null;
        }   // Ennd Method GetSchema

        public void ReadXml(XmlReader reader)
        {
            this = reader.ReadElementContentAsString();
        }   // End Method ReadXml

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteString(this.ToString());
        }   // End Method WriteXml
        #endregion

    }   // End Struct CertificateType

}   // End Namespace SendEncryptedEmail.Model
