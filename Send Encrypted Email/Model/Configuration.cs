﻿using Caliburn.Micro;
using SendEncryptedEmail.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Xml.Serialization;

namespace SendEncryptedEmail.Model
{
    public class Configuration : PropertyChangedBase
    {
        #region Constants
        private const string _ConfigurationDirectory = "Send Encrypted Email";
        private const string _CertificateSubDirectory = "Certificate";
        private const string _ConfigurationFileName = "SendEncryptedEmailConfig.xml";
        private const double _DefaultWindowWidth = 1400;
        private const double _DefaultWindowHeight = 900;
        #endregion

        #region Variables
        private static Configuration _Instance = null;

        private int _LastCertificateId;
        private ObservableCollection<Certificate> _CertificateList;

        // Window Configuration
        private double _WindowTop;
        private double _WindowLeft;
        private double _WindowWidth;
        private double _WindowHeight;
        private WindowState _WindowState;
        #endregion

        #region Constructor
        private Configuration()
        {

        }   // End Method Configuration
        #endregion

        #region Properties
        [XmlIgnore]
        public static Configuration Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Configuration();
                    SetDefaults();
                }
                return _Instance;
            }
        }   // End Property Instance

        [XmlArray("CertificateList")]
        [XmlArrayItem("Certificate")]
        public ObservableCollection<Certificate> CertificateList
        {
            get { return _CertificateList; }
            set { _CertificateList = value; }
        }   // End Property CertificateList

        public double WindowTop
        {
            get { return _WindowTop; }
            set { _WindowTop = value; }
        }   // End Property WindowTop

        public double WindowLeft
        {
            get { return _WindowLeft; }
            set { _WindowLeft = value; }
        }   // End Property WindowLeft

        public double WindowWidth
        {
            get { return _WindowWidth; }
            set { _WindowWidth = value; }
        }   // End Property WindowWidth

        public double WindowHeight
        {
            get { return _WindowHeight; }
            set { _WindowHeight = value; }
        }   // End Property WindowHeight

        public WindowState WindowState
        {
            get { return _WindowState; }
            set { _WindowState = value; }
        }   // End Property WindowState
        #endregion

        #region Methods
        public static void Initialise()
        {
            _Instance = Load();
            Instance._LastCertificateId = 0;
            if (Instance.CertificateList != null)
            {
                foreach (Certificate certificate in Instance.CertificateList)
                {
                    if (certificate.Id > Instance._LastCertificateId)
                        Instance._LastCertificateId = certificate.Id;
                }
            }
            else
                Instance.CertificateList = new ObservableCollection<Certificate>();
        }   // End Method Initialise

        public static void AddCertificate(Certificate certificate)
        {
            Instance._LastCertificateId += 1;
            certificate.Id = Instance._LastCertificateId;
            Instance.CertificateList.Add(certificate);
        }   // End Method AddCertificate

        public static void RemoveCertificate(Certificate certificate)
        {
            Instance.CertificateList.Remove(certificate);
        }   // End Method AddCertificate

        public static void Save()
        {
            Save(_Instance);
        }   // End Method Save

        public static void Save(Configuration configuration)
        {
            XmlSerializer serializer;
            string filePath;
            string fileName;

            filePath = GetConfigurationPath();
            if (string.IsNullOrWhiteSpace(filePath))
                return;

            fileName = Path.Combine(filePath, _ConfigurationFileName);

            serializer = new XmlSerializer(typeof(Configuration));
            try
            {
                using (FileStream stream = new FileStream(fileName, FileMode.Create))
                {
                    using (TextWriter textWriter = new StreamWriter(stream))
                    {
                        serializer.Serialize(textWriter, configuration);
                    }
                }
            }
            catch (Exception ex)
            {
                SendEncryptedEmailMessageBox.ShowError($"Failed to save Send Encrypted Email Configuration file.\n\n{ex.Message}");
            }

        }   // End Method Save

        public static Configuration Load()
        {
            XmlSerializer serializer;
            Configuration configuration;
            string filePath;
            string fileName;

            filePath = GetConfigurationPath();
            if (string.IsNullOrWhiteSpace(filePath))
                return null;

            fileName = Path.Combine(filePath, _ConfigurationFileName);

            if (!File.Exists(fileName))
                return null;

            using (FileStream stream = new FileStream(fileName, FileMode.Open))
            {
                serializer = new XmlSerializer(typeof(Configuration));
                using (TextReader textReader = new StreamReader(stream))
                {
                    try
                    {
                        configuration = (Configuration)serializer.Deserialize(textReader);
                    }
                    catch (Exception ex)
                    {
                        SendEncryptedEmailMessageBox.ShowError($"Failed to load Send Encrypted Email Configuration file.\n\n{ex.Message}");
                        return null;
                    }
                }

                return configuration;
            }
        }   // End Method Load

        public static string GetConfigurationPath()
        {
            string filePath;

            filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), _ConfigurationDirectory);
            if (!Directory.Exists(filePath))
            {
                if (!CreateConfigurationDirectory(filePath))
                    return null;
            }
            return filePath;

        }   // End Method GetConfigurationPath

        public static string GetCertificatePath()
        {
            string filePath;

            filePath = Path.Combine(GetConfigurationPath(), _CertificateSubDirectory);
            if (!Directory.Exists(filePath))
            {
                if (!CreateCertificateDirectory(filePath))
                    return null;
            }
            return filePath;

        }   // End Method GetConfigurationPath

        #region Private Methods
        private static bool CreateConfigurationDirectory(string filePath)
        {
            try
            {
                Directory.CreateDirectory(filePath);
            }
            catch (Exception ex)
            {
                SendEncryptedEmailMessageBox.ShowError($"Failed to create Send Encrypted Email Configuration directory.\n\n{ex.Message}");
                return false;
            }
            return true;

        }   // End Method CreateConfigurationDirectory

        private static bool CreateCertificateDirectory(string filePath)
        {
            try
            {
                Directory.CreateDirectory(filePath);
            }
            catch (Exception ex)
            {
                SendEncryptedEmailMessageBox.ShowError($"Failed to create Send Encrypted Email Certificate directory.\n\n{ex.Message}");
                return false;
            }
            return true;

        }   // End Method CreateCertificateDirectory

        private static void SetDefaults()
        {
            _Instance.WindowWidth = _DefaultWindowWidth;
            _Instance.WindowHeight = _DefaultWindowHeight;
            _Instance.WindowLeft = (SystemParameters.WorkArea.Width - _Instance.WindowWidth) / 2;
            _Instance.WindowTop = (SystemParameters.WorkArea.Height - _Instance.WindowHeight) / 2;
            _Instance.WindowState = WindowState.Normal;
        }   // End Method SetDefaults
        #endregion
        #endregion

    }   // End Class Configuration

}   // End Namespace SendEncryptedEmail.Model

