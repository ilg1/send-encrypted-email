﻿using Caliburn.Micro;
using ILG.Application;
using SendEncryptedEmail.Model;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace SendEncryptedEmail.ViewModel
{
    public class ContactManagementViewModel : Screen, IViewAware
    {
        #region Variables
        private Window _DialogWindow;
        private event EventHandler<ViewAttachedEventArgs> _ViewAttached;

        private Contact _SelectedContact;
        #endregion

        #region Constructor

        #endregion

        #region Properties
        public AddressBook AddressBook
        {
            get { return AddressBook.Instance; }
        }   // End Property AddressBook

        public Contact SelectedContact
        {
            get { return _SelectedContact; }
            set
            {
                _SelectedContact = value;
                NotifyOfPropertyChange(() => SelectedContact);
                NotifyOfPropertyChange(() => CanEditContact);
                NotifyOfPropertyChange(() => CanRemoveContact);
            }
        }   // End Property SelectedContact

        public bool CanEditContact 
        { 
            get { return _SelectedContact != null; }
        }   // End Property CanEditContact

        public bool CanRemoveContact
        {
            get { return _SelectedContact != null; }
        }   // End Property CanRemoveContact
        #endregion

        #region Methods
        public void AddContact()
        {
            ContactViewModel contactView;

            contactView = new ContactViewModel();
            if (WindowManagement.ShowDialog(contactView))
                NotifyOfPropertyChange(() => AddressBook);

        }   // End Method AddContact

        public void EditContact()
        {
            ContactViewModel contactView;

            contactView = new ContactViewModel(_SelectedContact);
            if (WindowManagement.ShowDialog(contactView))
                NotifyOfPropertyChange(() => AddressBook);

        }   // End Method EditContact

        public void RemoveContact()
        {
            if (SendEncryptedEmailMessageBox.Show($"Are you sure you wish to delete the contact record for {_SelectedContact.Name}?",
                MessageBoxButton.YesNoCancel, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                AddressBook.RemoveContact(_SelectedContact);
                NotifyOfPropertyChange(() => AddressBook);
            }
        }   // End Method RemoveContact

        public void ContactDoubleClick()
        {
            EditContact();
        }   // End Method ContactDoubleClick
        #endregion

        #region WindowStuff
        public void AttachView(object view, object context = null)
        {
            _DialogWindow = view as Window;
            if (_ViewAttached != null)
                _ViewAttached(this, new ViewAttachedEventArgs() { Context = context, View = view });
        }   // End Method AttachedView

        public override object GetView(object context = null)
        {
            return _DialogWindow;
        }   // End Method GetView

        protected override void OnInitialize()
        {
            _DialogWindow.Title = "Contact Management";
            _DialogWindow.Loaded += DialogWindowLoaded;

        }   // End Method OnInitialize

        private void DialogWindowLoaded(object sender, EventArgs e)
        {

            // Add code to be run after the window has been loaded

        }   // End Method DialogWindowLoaded
        #endregion

    }   // End Class ContactManagementViewModel

}   // End Namespace SendEncryptedEmail.ViewModel
