﻿using Caliburn.Micro;
using ILG.Application;
using SendEncryptedEmail.Model;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace SendEncryptedEmail.ViewModel
{
    public enum AddressType
    {
        To,
        Cc,
        Bcc
    }   // End Enum AddressType

    public class AddressBookViewModel : Screen, IViewAware
    {
        #region Variables
        private Window _DialogWindow;
        private event EventHandler<ViewAttachedEventArgs> _ViewAttached;

        private Contact _SelectedContact;
        private ObservableCollection<Contact> _ToList;
        private ObservableCollection<Contact> _CcList;
        private ObservableCollection<Contact> _BccList;
        private AddressType _ActiveAddressType;
        #endregion

        #region Constructor
        public AddressBookViewModel(ObservableCollection<Contact> toList, 
            ObservableCollection<Contact> ccList, ObservableCollection<Contact> bccList, AddressType addressType = AddressType.To)
        {
            _ToList = toList;
            _CcList = ccList;
            _BccList = bccList;
            _ActiveAddressType = addressType;
        }   // End Constructor AddressBookViewModel
        #endregion

        #region Properties
        public AddressBook AddressBook 
        { 
            get { return AddressBook.Instance; }
        }   // End Property AddressBook

        public Contact SelectedContact 
        { 
            get { return _SelectedContact; }
            set
            {
                _SelectedContact = value;
                NotifyOfPropertyChange(() => SelectedContact);
                NotifyOfPropertyChange(() => CanToEmail);
                NotifyOfPropertyChange(() => CanCcEmail);
                NotifyOfPropertyChange(() => CanBccEmail);
            }
        }   // End Property SelectedContact

        public ObservableCollection<Contact> ToList 
        { 
            get { return _ToList; }
            set
            {
                _ToList = value;
                NotifyOfPropertyChange(() => ToList);
            }
        }   // End Property ToList

        public ObservableCollection<Contact> CcList
        {
            get { return _CcList; }
            set
            {
                _CcList = value;
                NotifyOfPropertyChange(() => CcList);
            }
        }   // End Property CcList

        public ObservableCollection<Contact> BccList
        {
            get { return _BccList; }
            set
            {
                _BccList = value;
                NotifyOfPropertyChange(() => BccList);
            }
        }   // End Property BccList

        public AddressType ActiveAddressType 
        { 
            get { return _ActiveAddressType; }
            set
            {
                _ActiveAddressType = value;
                NotifyOfPropertyChange(() => ActiveAddressType);
            }
        }   // End Property ActiveAddressType

        public bool CanToEmail 
        { 
            get { return _SelectedContact != null; }
        }   // End Property CanToEmail

        public bool CanCcEmail
        {
            get { return _SelectedContact != null; }
        }   // End Property CanCcEmail

        public bool CanBccEmail
        {
            get { return _SelectedContact != null; }
        }   // End Property CanBccEmail
        #endregion

        #region Methods
        public void AddContactToActiveList()
        {
            switch (_ActiveAddressType)
            {
                case AddressType.To:
                    ToEmail();
                    break;
                case AddressType.Cc:
                    CcEmail();
                    break;
                case AddressType.Bcc:
                    BccEmail();
                    break;
            }
        }   // End Method AddContactToActiveList

        public void ToEmail()
        {
            _ToList.Add(_SelectedContact);
        }   // End Method ToEmail

        public void CcEmail()
        {
            _CcList.Add(_SelectedContact);
        }   // End Method CcEmail

        public void BccEmail()
        {
            _BccList.Add(_SelectedContact);
        }   // End Method BccEmail

        public void ManageContacts()
        {
            ContactManagementViewModel contactManagementView;

            contactManagementView = new ContactManagementViewModel();
            if (WindowManagement.ShowDialog(contactManagementView))
                NotifyOfPropertyChange(() => AddressBook);

        }   // End Method ManageContacts

        public void UpdateEmailAddress()
        {
            _DialogWindow.DialogResult = true;
            _DialogWindow.Close();
        }   // End Method UpdateEmailAddress
        #endregion

        #region WindowStuff
        public void AttachView(object view, object context = null)
        {
            _DialogWindow = view as Window;
            if (_ViewAttached != null)
                _ViewAttached(this, new ViewAttachedEventArgs() { Context = context, View = view });
        }   // End Method AttachedView

        public override object GetView(object context = null)
        {
            return _DialogWindow;
        }   // End Method GetView

        protected override void OnInitialize()
        {
            _DialogWindow.Title = "Address Book";
            _DialogWindow.Loaded += DialogWindowLoaded;

        }   // End Method OnInitialize

        private void DialogWindowLoaded(object sender, EventArgs e)
        {

            // Add code to be run after the window has been loaded

        }   // End Method DialogWindowLoaded
        #endregion

    }   // End Class AddressBookViewModel

}   // End Namespace SendEncryptedEmail.ViewModel
