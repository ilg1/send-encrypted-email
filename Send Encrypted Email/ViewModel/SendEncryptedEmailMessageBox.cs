﻿using ILG.Events;
using SendEncryptedEmail.Events;
using System.Threading;
using System.Windows;

namespace SendEncryptedEmail.ViewModel
{
    public class SendEncryptedEmailMessageBox
    {
        #region Variables
        private static string _MessageBoxTitle = "Send Encrypted Email";
        #endregion

        #region Properties
        public static string MessageBoxTitle
        {
            get { return _MessageBoxTitle; }
            set { _MessageBoxTitle = value; }
        }   // End Property MessageBoxTitle
        #endregion

        #region Methods
        public static void Show(DisplayMessageEvent message)
        {
            message.Result = MessageBox.Show(message.Message,
                _MessageBoxTitle, message.ButtonStyle, message.ImageStyle);
            if (message.NotifyComplete != null)
                message.NotifyComplete.Set();
        }   // End Method Show

        public static MessageBoxResult Show(string message,
            MessageBoxButton buttonStyle = MessageBoxButton.OK, MessageBoxImage imageStyle = MessageBoxImage.None)
        {
            return MessageBox.Show(message, _MessageBoxTitle, buttonStyle, imageStyle);
        }   // End Method Show

        public static MessageBoxResult Show(string message, string title,
            MessageBoxButton buttonStyle = MessageBoxButton.OK, MessageBoxImage imageStyle = MessageBoxImage.None)
        {
            return MessageBox.Show(message, title, buttonStyle, imageStyle);
        }   // End Method Show

        public static MessageBoxResult WaitShow(string message,
            MessageBoxButton buttonStyle = MessageBoxButton.OK, MessageBoxImage imageStyle = MessageBoxImage.None)
        {
            DisplayMessageEvent messageBoxEvent;

            messageBoxEvent = new DisplayMessageEvent(message, buttonStyle, imageStyle, new ManualResetEvent(false));
            EventsManager.RaiseEvent(messageBoxEvent);

            messageBoxEvent.NotifyComplete.WaitOne();

            return messageBoxEvent.Result;

        }   // End Method WaitShow

        public static MessageBoxResult ShowError(string message)
        {
            return MessageBox.Show(message, _MessageBoxTitle, MessageBoxButton.OK, MessageBoxImage.Error);
        }   // End Method ShowError

        public static void EventShow(string message,
            MessageBoxButton buttonStyle = MessageBoxButton.OK, MessageBoxImage imageStyle = MessageBoxImage.None)
        {
            EventsManager.RaiseEvent(new DisplayMessageEvent(message, buttonStyle, imageStyle, new ManualResetEvent(false)));
        }   // End Method WaitShow
        #endregion

    }   // End Class SendEncryptedEmailMessageBox
}
