﻿using Caliburn.Micro;
using ILG.Application;
using ILG.FileOperations;
using SendEncryptedEmail.Model;
using System;
using System.Windows;

namespace SendEncryptedEmail.ViewModel
{
    public class ContactViewModel : Screen, IViewAware
    {
        #region Variables
        private Window _DialogWindow;
        private event EventHandler<ViewAttachedEventArgs> _ViewAttached;
        private Contact _OriginalContact;
        private Contact _Contact;
        private bool _IsNew;
        #endregion

        #region Constructor
        public ContactViewModel(Contact contact = null)
        {
            _OriginalContact = contact;
            if (contact == null)
            {
                _Contact = new Contact();
                _IsNew = true;
            }
            else
            {
                _Contact = contact.DeepCopy();
                _IsNew = false;
            }

            _Contact.PropertyChanged += ContactPropertyChanged;

        }   // End Constructor ContactViewModel
        #endregion

        #region Properties
        public Contact Contact 
        { 
            get { return _Contact; }
            set
            {
                _Contact = value;
                NotifyOfPropertyChange(() => Contact);
            }
        }   // End Property Contact

        public bool CanSaveContact 
        { 
            get { return _Contact.IsValid(); }
        }   // End Property CanSaveContact
        #endregion

        #region Methods
        public void SelectCertificate()
        {
            CertificateManagementViewModel certificateManagementView;

            certificateManagementView = new CertificateManagementViewModel(CertificateType.Encryption);
            if (WindowManagement.ShowDialog(certificateManagementView))
            {
                _Contact.Certificate = certificateManagementView.SelectedCertificate;
            }

        }   //  End Method SelectCertificate

        public void SaveContact()
        {
            _Contact.PropertyChanged -= ContactPropertyChanged;

            if (_IsNew)
            {
                AddressBook.AddContact(_Contact);
            }
            else
            {
                _OriginalContact.Name = _Contact.Name;
                _OriginalContact.Email = _Contact.Email;
                _OriginalContact.Certificate = _Contact.Certificate;
            }

            AddressBook.Save();

            _DialogWindow.DialogResult = true;
            _DialogWindow.Close();

        }   // End Method SaveContact
        #endregion

        #region Event Handlers
        private void ContactPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => CanSaveContact);
        }   // End Event Handler ContactPropertyChanged
        #endregion

        #region WindowStuff
        public void AttachView(object view, object context = null)
        {
            _DialogWindow = view as Window;
            if (_ViewAttached != null)
                _ViewAttached(this, new ViewAttachedEventArgs() { Context = context, View = view });
        }   // End Method AttachedView

        public override object GetView(object context = null)
        {
            return _DialogWindow;
        }   // End Method GetView

        protected override void OnInitialize()
        {
            _DialogWindow.Title = "Contact";
            _DialogWindow.Loaded += DialogWindowLoaded;

        }   // End Method OnInitialize

        private void DialogWindowLoaded(object sender, EventArgs e)
        {

            // Add code to be run after the window has been loaded

        }   // End Method DialogWindowLoaded
        #endregion

    }   // End Class ContactViewModel

}   // End Namespace SendEncryptedEmail.ViewModel
