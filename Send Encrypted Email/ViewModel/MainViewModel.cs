﻿using Caliburn.Micro;
using EASendMail;
using ILG.Application;
using ILG.Events;
using ILG.FileOperations;
using SendEncryptedEmail.Model;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SendEncryptedEmail.ViewModel
{
    [Export(typeof(MainViewModel))]
    public class MainViewModel : Screen, IViewAware
    {
        #region Constants
        private const string _EaSendMailLicenceKey = "ES-E1646458156-01046-E919DF76AC714EV8-28TD65U55U5DDC53";
        private const string _TennantId = "7e3a8855-d847-4036-a440-e475b52b4bc1";
        private const string _ClientId = "fb595058-b21d-4d37-9199-19148f00640b";
        private const string _ClientSecret = "Qtn7Q~khV46~7mxJmmyXqLoIBgIYpDtWeuOJu";
        private const string _MsGraphScope = "https://graph.microsoft.com/Mail.Send%20offline_access%20email%20openid";
        private const string _MsGraphSendMail = "https://graph.microsoft.com/v1.0/me/sendMail";
        private readonly string _AuthUri = $"https://login.microsoftonline.com/{_TennantId}/oauth2/v2.0/authorize";
        private readonly string _TokenUri = $"https://login.microsoftonline.com/{_TennantId}/oauth2/v2.0/token";
        #endregion

        #region Variables
        private Window _DialogWindow;
        private event EventHandler<ViewAttachedEventArgs> _ViewAttached;

        private bool _IsSignedIn;
        private string _UserEmail;
        private string _UserToken;

        private ObservableCollection<Contact> _ToList;
        private ObservableCollection<Contact> _CcList;
        private ObservableCollection<Contact> _BccList;
        private ObservableCollection<Attachment> _AttachmentList;

        private string _Subject;
        private string _Body;

        private bool _AddToSentItems;

        private bool _SignEmail;
        private Model.Certificate _SigningCertificate;
        private bool _EncryptEmail;
        #endregion

        #region Constructor
        [ImportingConstructor]
        public MainViewModel(IWindowManager windowManager, IEventAggregator events)
        {
            // Initialise the Events Manager and subscribe this class to receive events
            EventsManager.Initialise(events);
            EventsManager.Subscribe(this);

            // Initialise the Window Manager
            WindowManagement.Initialise(windowManager);

            InitialiseNewEmail();

        }   // End Constructor MainViewModel
        #endregion

        #region Properties
        public bool IsSignedIn
        {
            get { return _IsSignedIn; }
            set
            {
                _IsSignedIn = value;
                NotifyOfPropertyChange(() => IsSignedIn);
                NotifyOfPropertyChange(() => CanSendEmail);
            }
        }   // End Property IsSignedIn

        public string UserEmail 
        { 
            get { return _UserEmail; }
            set
            {
                _UserEmail = value;
                NotifyOfPropertyChange(() => UserEmail);
            }
        }   // End Property UserEmail

        public ObservableCollection<Contact> ToList
        {
            get { return _ToList; }
            set
            {
                _ToList = value;
                NotifyOfPropertyChange(() => ToList);
            }
        }   // End Property ToList

        public ObservableCollection<Contact> CcList
        {
            get { return _CcList; }
            set
            {
                _CcList = value;
                NotifyOfPropertyChange(() => CcList);
            }
        }   // End Property CcList

        public ObservableCollection<Contact> BccList
        {
            get { return _BccList; }
            set
            {
                _BccList = value;
                NotifyOfPropertyChange(() => BccList);
            }
        }   // End Property BccList

        public string Subject 
        { 
            get { return _Subject; }
            set
            {
                _Subject = value;
                NotifyOfPropertyChange(() => Subject);
                NotifyOfPropertyChange(() => CanSendEmail);
            }
        }   // End Property Subject

        public ObservableCollection<Attachment> AttachmentList
        {
            get { return _AttachmentList; }
            set
            {
                _AttachmentList = value;
                NotifyOfPropertyChange(() => AttachmentList);
            }
        }   // End Property AttachmentList

        public string Body 
        { 
            get { return _Body; }
            set
            {
                _Body = value;
                NotifyOfPropertyChange(() => Body);
            }
        }   // End Property Body

        public bool AddToSentItems
        { 
            get { return _AddToSentItems; }
            set
            {
                _AddToSentItems = value;
                NotifyOfPropertyChange(() => AddToSentItems);
            }
        }   // End Property AddToSentItems

        public bool SignEmail 
        { 
            get { return _SignEmail; }
            set
            {
                CertificateManagementViewModel certificateManagementView;

                _SignEmail = value;
                if (_SignEmail)
                {
                    certificateManagementView = new CertificateManagementViewModel(CertificateType.Signing);
                    if (WindowManagement.ShowDialog(certificateManagementView))
                        _SigningCertificate = certificateManagementView.SelectedCertificate;
                    else
                        _SignEmail = false;
                }
                else
                    _SigningCertificate = null;
                NotifyOfPropertyChange(() => SignEmail);
            }
        }   // End Property SignEmail

        public bool EncryptEmail
        {
            get { return _EncryptEmail; }
            set
            {
                _EncryptEmail = value;
                NotifyOfPropertyChange(() => EncryptEmail);
            }
        }   // End Property EncryptEmail

        public bool CanSendEmail 
        { 
            get 
            {
                if (!_IsSignedIn)
                    return false;

                if (_ToList.Count == 0 && _CcList.Count == 0 && _BccList.Count == 0)
                    return false;

                if (string.IsNullOrWhiteSpace(_Subject))
                    return false;

                return true;
            }
        }   // End Property CanSendEmail
        #endregion

        #region Methods
        public void SignIn()
        {
            SignInToOffice365();
        }   // End Method SignIn

        public void ToEmail()
        {
            LaunchAddressBookView(AddressType.To);
        }   // End Method ToEmail

        public void CcEmail()
        {
            LaunchAddressBookView(AddressType.Cc);
        }   // End Method CcEmail

        public void BccEmail()
        {
            LaunchAddressBookView(AddressType.Bcc);
        }   // End Method BccEmail

        public void ManageCertificates()
        {
            CertificateManagementViewModel CertificateManagementView;

            CertificateManagementView = new CertificateManagementViewModel();
            WindowManagement.ShowDialog(CertificateManagementView);

        }   // End Method ManageCertificates

        public void AddAttachment()
        {
            BrowseForOpenFile browseForFile;
            Attachment attachment;
            string fileName;

            browseForFile = new BrowseForOpenFile();
            browseForFile.Filter = "All Files (*.*)|*.*";
            browseForFile.Title = "Select Attachment";
            fileName = browseForFile.ShowDialog();
            if (string.IsNullOrWhiteSpace(fileName))
                return;

            attachment = new Attachment();
            attachment.Load(fileName);

            _AttachmentList.Add(attachment);
            
        }   // End Method AddAttachment

        public void SendEmail()
        {
            SmtpServer smtpServer;
            SmtpMail smtpMail;
            SmtpClient smtpClient;
            bool missingCertificates;

            missingCertificates = false;
            if (_EncryptEmail)
            {
                foreach (Contact contact in _ToList)
                    missingCertificates |= !contact.HasCertificate;

                // CC Addresses
                foreach (Contact contact in _CcList)
                    missingCertificates |= !contact.HasCertificate;

                // BCC Addresses
                foreach (Contact contact in _BccList)
                    missingCertificates |= !contact.HasCertificate;

                if (missingCertificates)
                {
                    if (SendEncryptedEmailMessageBox.Show("Not all receipients have a certificate loaded for encryption.\nDo you wish to send to these recipients without encryption?",
                        MessageBoxButton.YesNoCancel, MessageBoxImage.Question) != MessageBoxResult.Yes)
                        return;
                }
            }

            // Office365 Ms Graph API server address
            smtpServer = new SmtpServer(_MsGraphSendMail);

            // use Ms Graph API protocol
            smtpServer.Protocol = ServerProtocol.MsGraphApi;
            // enable SSL connection
            smtpServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

            // use SMTP OAUTH 2.0 authentication
            smtpServer.AuthType = SmtpAuthType.XOAUTH2;
            // set user authentication
            smtpServer.User = _UserEmail;
            // use access token as password
            smtpServer.Password = _UserToken;

            smtpMail = new SmtpMail(_EaSendMailLicenceKey);

            // From Address
            smtpMail.From = _UserEmail;
            if (_SignEmail)
            {
                try
                {
                    smtpMail.From.Certificate.Load(_SigningCertificate.GetFileName(), _SigningCertificate.Password,
                        EASendMail.Certificate.CertificateKeyLocation.CRYPT_USER_KEYSET);
                }
                catch (Exception exp)
                {
                    SendEncryptedEmailMessageBox.ShowError($"Unable to load Signing Certificate.\n\n{exp.Message}");
                }
            }

            // To Addresses
            foreach (Contact contact in _ToList)
                smtpMail.To.Add(contact.ContactToMailAddress(_EncryptEmail));

            // CC Addresses
            foreach (Contact contact in _CcList)
                smtpMail.Cc.Add(contact.ContactToMailAddress(_EncryptEmail));

            // BCC Addresses
            foreach (Contact contact in _BccList)
                smtpMail.Bcc.Add(contact.ContactToMailAddress(_EncryptEmail));

            smtpMail.Subject = _Subject;
            smtpMail.TextBody = _Body;

            if (_EncryptEmail)
                smtpMail.EncryptionAlgorithm = EncryptionAlgorithmType.ENCRYPTION_ALGORITHM_RC2;

            // Add any attachments
            foreach (Attachment attachment in _AttachmentList)
                smtpMail.AddAttachment(attachment.Path);

            smtpClient = new SmtpClient();
            smtpClient.SaveCopy = _AddToSentItems;
            smtpClient.SendMail(smtpServer, smtpMail);

            SendEncryptedEmailMessageBox.Show("The email has been submitted to server successfully!");

            InitialiseNewEmail();

        }   // End Method SendEmail

        #region Private Methods
        private void InitialiseNewEmail()
        {
            ToList = new ObservableCollection<Contact>();
            CcList = new ObservableCollection<Contact>();
            BccList = new ObservableCollection<Contact>();
            AttachmentList = new ObservableCollection<Attachment>();

            Subject = "";
            Body = "";

            AddToSentItems = true;

            SignEmail = false;
            EncryptEmail = false;

        }   // End Method InitialiseNewEmail

        private void LaunchAddressBookView(AddressType addressType)
        {
            AddressBookViewModel addressBookView;

            addressBookView = new AddressBookViewModel(_ToList, _CcList, _BccList, addressType);
            if (WindowManagement.ShowDialog(addressBookView))
            {
                NotifyOfPropertyChange(() => CanSendEmail);
            }
        }   // End Method LaunchAddressBookView

        private async void SignInToOffice365()
        {
            HttpListener httpListener;
            HttpListenerContext httpListenerContext;
            HttpListenerResponse httpListenerResponse;
            OAuthResponseParser parser;
            Stream responseOutput;
            Task responseTask;
            string redirectUri;
            string authorizationRequest;
            string responseString;
            string responseText;
            string code;
            byte[] buffer;

            // Clear out variables
            IsSignedIn = false;
            _UserEmail = null;
            _UserToken = null;

            // Creates a redirect URI using an available port on the loopback address.
            redirectUri = string.Format("http://127.0.0.1:{0}/", GetRandomUnusedPort());

            // Creates an HttpListener to listen for requests on that redirect URI.
            httpListener = new HttpListener();
            httpListener.Prefixes.Add(redirectUri);
            httpListener.Start();

            // Creates the OAuth 2.0 authorization request.
            authorizationRequest = string.Format("{0}?response_type=code&scope={1}&redirect_uri={2}&client_id={3}&prompt=login",
                _AuthUri, _MsGraphScope, Uri.EscapeDataString(redirectUri), _ClientId);

            // Opens request in the browser.
            Process.Start(authorizationRequest);

            // Waits for the OAuth authorization response.
            httpListenerContext = await httpListener.GetContextAsync();

            // Bring the Main Window nack into focus
            WindowManagement.ActivateWindow(_DialogWindow);

            // Sends an HTTP response to the browser.
            httpListenerResponse = httpListenerContext.Response;
            responseString = string.Format("<html><head></head><body>Please return to the app and close current window.</body></html>");
            buffer = Encoding.UTF8.GetBytes(responseString);
            httpListenerResponse.ContentLength64 = buffer.Length;
            responseOutput = httpListenerResponse.OutputStream;
            responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) =>
            {
                responseOutput.Close();
                httpListener.Stop();
            });

            // Checks for errors.
            if (httpListenerContext.Request.QueryString.Get("error") != null)
            {
                Console.WriteLine(string.Format("OAuth authorization error: {0}.", httpListenerContext.Request.QueryString.Get("error")));
                return;
            }

            if (httpListenerContext.Request.QueryString.Get("code") == null)
            {
                Console.WriteLine("Malformed authorization response. " + httpListenerContext.Request.QueryString);
                return;
            }

            // extracts the code
            code = httpListenerContext.Request.QueryString.Get("code");

            responseText = await RequestAccessToken(code, redirectUri);

            parser = new OAuthResponseParser();
            parser.Load(responseText);

            UserEmail = parser.EmailInIdToken;
            _UserToken = parser.AccessToken;

            IsSignedIn = true;

        }   // End Method SignInToOffice365

        private async Task<string> RequestAccessToken(string code, string redirectUri)
        {
            HttpWebRequest tokenRequest;
            HttpWebResponse response;
            WebResponse tokenResponse;
            Stream stream;
            string tokenRequestBody;
            string responseText;
            byte[] byteVersion;

            // builds the  request
            tokenRequestBody = string.Format("code={0}&redirect_uri={1}&client_id={2}&grant_type=authorization_code",
                code, Uri.EscapeDataString(redirectUri), _ClientId);

            // sends the request
            tokenRequest = (HttpWebRequest)WebRequest.Create(_TokenUri);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = byteVersion.Length;

            stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(byteVersion, 0, byteVersion.Length);
            stream.Close();

            try
            {
                // gets the response
                tokenResponse = await tokenRequest.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    return await reader.ReadToEndAsync();
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            responseText = await reader.ReadToEndAsync();
                            SendEncryptedEmailMessageBox.EventShow($"Failed to get Access Token to be able to send emails.\n\n{responseText}",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }

                throw ex;
            }

        }   // End Method RequestAccessToken

        private static int GetRandomUnusedPort()
        {
            TcpListener listener;
            int port;

            listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();

            return port;

        }   // End Method GetRandomUnusedPort
        #endregion
        #endregion

        #region WindowStuff
        public void AttachView(object view, object context = null)
        {
            _DialogWindow = view as Window;
            if (_ViewAttached != null)
                _ViewAttached(this, new ViewAttachedEventArgs() { Context = context, View = view });
        }   // End Method AttachedView

        public override object GetView(object context = null)
        {
            return _DialogWindow;
        }   // End Method GetView

        protected override void OnInitialize()
        {
            _DialogWindow.Title = "Send Encrypted Email";
            _DialogWindow.Closing += DialogWindowClosing;
            _DialogWindow.Loaded += DialogWindowLoaded;

        }   // End Method OnInitialize

        private void DialogWindowLoaded(object sender, EventArgs e)
        {
            // Load the configuration
            Configuration.Initialise();

            InitialiseWindow();

            // Load the Address Book
            AddressBook.Initialise();

            foreach (Model.Certificate certificate in Configuration.Instance.CertificateList)
            {
                foreach (Contact contact in AddressBook.Instance.ContactList)
                {
                    if (contact.HasCertificate && contact.Certificate.Id == certificate.Id)
                        contact.Certificate = certificate;
                }
            }

        }   // End Method DialogWindowLoaded

        private void InitialiseWindow()
        {
            // Restore the Window to its position and size from last run
            if (Configuration.Instance.WindowHeight > SystemParameters.WorkArea.Height)
            {
                Configuration.Instance.WindowHeight = SystemParameters.WorkArea.Height;
                Configuration.Instance.WindowTop = 0;
            }
            if (Configuration.Instance.WindowWidth > SystemParameters.WorkArea.Width)
            {
                Configuration.Instance.WindowWidth = SystemParameters.WorkArea.Width;
                Configuration.Instance.WindowLeft = 0;
            }
            if ((Configuration.Instance.WindowTop + Configuration.Instance.WindowHeight) > SystemParameters.WorkArea.Height)
                Configuration.Instance.WindowTop -= (Configuration.Instance.WindowTop +
                        Configuration.Instance.WindowHeight - SystemParameters.WorkArea.Height);
            if ((Configuration.Instance.WindowLeft + Configuration.Instance.WindowWidth) > SystemParameters.WorkArea.Width)
                Configuration.Instance.WindowLeft -= (Configuration.Instance.WindowLeft +
                        Configuration.Instance.WindowWidth - SystemParameters.WorkArea.Width);
            if (Configuration.Instance.WindowTop < 0)
                Configuration.Instance.WindowTop = 0;
            if (Configuration.Instance.WindowLeft < 0)
                Configuration.Instance.WindowLeft = 0;

            _DialogWindow.Top = Configuration.Instance.WindowTop;
            _DialogWindow.Left = Configuration.Instance.WindowLeft;
            _DialogWindow.Width = Configuration.Instance.WindowWidth;
            _DialogWindow.Height = Configuration.Instance.WindowHeight;

            if (Configuration.Instance.WindowState == WindowState.Maximized)
                _DialogWindow.WindowState = WindowState.Maximized;
        }   // End Method InitialiseWindow

        public override void CanClose(Action<bool> callback)
        {
            if (_DialogWindow.WindowState == WindowState.Minimized)
                Configuration.Instance.WindowState = WindowState.Normal;
            else
                Configuration.Instance.WindowState = _DialogWindow.WindowState;

            if (Configuration.Instance.WindowState == WindowState.Normal)
            {
                Configuration.Instance.WindowHeight = _DialogWindow.ActualHeight;
                Configuration.Instance.WindowWidth = _DialogWindow.ActualWidth;
                Configuration.Instance.WindowTop = _DialogWindow.Top;
                Configuration.Instance.WindowLeft = _DialogWindow.Left;
            }

            Configuration.Save();

            AddressBook.Save();

            base.CanClose(callback);
        }   // End Method CanClose

        private void DialogWindowClosing(object sender, CancelEventArgs e)
        {
            // Finalise the Events Manager Thread(s)
            EventsManager.StopEventThread();

            // Add code to be run before the Main Window closes

        }   // End Method DialogWindowClosing
        #endregion

    }   // End Class MainViewModel

}   // End Namespace SendEncryptedEmail.ViewModel
