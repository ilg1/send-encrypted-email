﻿using Caliburn.Micro;
using ILG.Controls;
using ILG.FileOperations;
using SendEncryptedEmail.Model;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using EA = EASendMail;

namespace SendEncryptedEmail.ViewModel
{
    public class CertificateViewModel : Screen, IViewAware
    {
        #region Variables
        private Window _DialogWindow;
        private event EventHandler<ViewAttachedEventArgs> _ViewAttached;
        private PasswordBox _Password;
        private Certificate _OriginalCertificate;
        private Certificate _Certificate;
        private string _FileName;
        private bool _IsNew;
        #endregion

        #region Constructor
        public CertificateViewModel(Certificate certificate = null)
        {
            _OriginalCertificate = certificate;
            
            if (certificate == null)
            {
                _Certificate = new Certificate();
                _Certificate.CertificateType = CertificateType.Encryption;
                _IsNew = true;
            }
            else
            {
                _Certificate = certificate.DeepCopy();
                _IsNew = false;
            }

            _Certificate.PropertyChanged += CertificatePropertyChanged;

        }   // End Constructor CertificateViewModel
        #endregion

        #region Properties
        public Certificate Certificate
        {
            get { return _Certificate; }
            set
            {
                _Certificate = value;
            }
        }   // End Property Certificate

        public bool IsNew 
        { 
            get { return _IsNew; }
        }   // End Property IsNew

        public string FileName 
        { 
            get { return _FileName; }
            set
            {
                _FileName = value;
                NotifyOfPropertyChange(() => FileName);
                NotifyOfPropertyChange(() => CanSaveCertificate);
            }
        }   // End Property FileName

        public bool CanSaveCertificate 
        { 
            get 
            { 
                if (_IsNew && string.IsNullOrWhiteSpace(_FileName))
                    return false;
                return _Certificate.CertificateIsValid(); 
            }
        }   // End Property CanSaveCertificate
        #endregion

        #region Methods
        public void BrowseCertificate()
        {
            BrowseForOpenFile browseForFile;
            string fileName;

            browseForFile = new BrowseForOpenFile();
            if (_Certificate.CertificateType == CertificateType.Signing)
                browseForFile.Filter = "Certificate Files (*.pfx)|*.pfx|All Files (*.*)|*.*";
            else
                browseForFile.Filter = "Certificate Files (*.cer,*.crt)|*.cer;*.crt|All Files (*.*)|*.*";
            browseForFile.Title = "Select Certificate";
            fileName = browseForFile.ShowDialog();
            if (string.IsNullOrWhiteSpace(fileName))
                return;

            FileName = fileName;
            CheckCertificate();

            _Certificate.Extension = Path.GetExtension(fileName).Replace(".", "");

        }   // End Method BrowseCertificate

        public void SaveCertificate()
        {
            string newFileName;

            _Certificate.PropertyChanged -= CertificatePropertyChanged;

            // Add the Certificate if it is new
            if (_IsNew)
                Configuration.AddCertificate(_Certificate);

            if (!string.IsNullOrWhiteSpace(_FileName))
            {
                newFileName = _Certificate.GetFileName();
                if (newFileName == null)
                {
                    if (_Certificate.Id == 0)
                        Configuration.RemoveCertificate(_Certificate);
                    SendEncryptedEmailMessageBox.ShowError("Failed to create a copy of the Certificate");
                    return;
                }
                File.Copy(_FileName, newFileName);
            }

            if (_OriginalCertificate != null)
            {
                _OriginalCertificate.Name = _Certificate.Name;
                _OriginalCertificate.Password = _Certificate.Password;
                _OriginalCertificate.Extension = _Certificate.Extension;
                _OriginalCertificate.ExpiryDate = _Certificate.ExpiryDate;
            }

            Configuration.Save();

            _DialogWindow.DialogResult = true;
            _DialogWindow.Close();
        }   // End Method SaveCertificate

        #region Private Methods
        private void CheckCertificate()
        {
            EA.Certificate eaCertificate;

            if (string.IsNullOrWhiteSpace(_FileName))
                return;

            if (_Certificate.CertificateType == CertificateType.Signing && string.IsNullOrWhiteSpace(_Password.Password))
                return;

            eaCertificate = new EA.Certificate();
            try
            {
                if (_Certificate.CertificateType == CertificateType.Signing)
                    eaCertificate.Load(_FileName, _Password.Password, EA.Certificate.CertificateKeyLocation.CRYPT_USER_KEYSET);
                else
                    eaCertificate.Load(_FileName);
            }
            catch (Exception ex)
            {
                SendEncryptedEmailMessageBox.ShowError($"Failed to check the selected Certificate.\n\n{ex.Message}");
                return;
            }

            _Certificate.ExpiryDate = eaCertificate.NotAfter;
            if (_Certificate.ExpiryDate < DateTime.Today)
                SendEncryptedEmailMessageBox.ShowError("Certificate is expired");

        }   // End Method CheckCertificate
        #endregion
        #endregion

        #region Event Handlers
        private void CertificatePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => CanSaveCertificate);
        }   // End Event Handler CertificatePropertyChanged

        private void PasswordLostFocus(object sender, RoutedEventArgs e)
        {
            _Certificate.Password = _Password.Password;
            CheckCertificate();
        }   // End Event Handler PasswordLostFocus
        #endregion

        #region WindowStuff
        public void AttachView(object view, object context = null)
        {
            _DialogWindow = view as Window;
            if (_ViewAttached != null)
                _ViewAttached(this, new ViewAttachedEventArgs() { Context = context, View = view });
        }   // End Method AttachedView

        public override object GetView(object context = null)
        {
            return _DialogWindow;
        }   // End Method GetView

        protected override void OnInitialize()
        {
            _DialogWindow.Title = "Certificate";
            _DialogWindow.Loaded += DialogWindowLoaded;

        }   // End Method OnInitialize

        private void DialogWindowLoaded(object sender, EventArgs e)
        {
            _Password = ControlHelper.FindChild<PasswordBox>(_DialogWindow, "PasswordBoxControl");
            if (_Password != null)
            {
                _Password.LostFocus += PasswordLostFocus;
                _Password.Password = _Certificate.Password;
            }
        }   // End Method DialogWindowLoaded
        #endregion

    }   // End Class CertificateViewModel

}   // End Namespace SendEncryptedEmail.ViewModel
