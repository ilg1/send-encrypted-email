﻿using Caliburn.Micro;
using ILG.Application;
using SendEncryptedEmail.Model;
using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace SendEncryptedEmail.ViewModel
{
    public class CertificateManagementViewModel : Screen, IViewAware
    {
        #region Variables
        private Window _DialogWindow;
        private event EventHandler<ViewAttachedEventArgs> _ViewAttached;
        private Certificate _SelectedCertificate;
        private CertificateType _FilterCertificateType;
        private bool _SelectMode;
        #endregion


        #region Constructor
        public CertificateManagementViewModel(bool selectMode = false)
        {
            _SelectMode = selectMode;
            _FilterCertificateType = CertificateType.None;
        }   // End Constructor CertificateManagementViewModel

        public CertificateManagementViewModel(CertificateType certificateType)
        {
            _SelectMode = true;
            _FilterCertificateType = certificateType;
        }   // End Constructor CertificateManagementViewModel
        #endregion

        #region Properties
        public ObservableCollection<Certificate> CertificateList
        {
            get 
            {
                ObservableCollection<Certificate> filteredCertificateList;
                if (_SelectMode && _FilterCertificateType != CertificateType.None)
                {
                    filteredCertificateList = new ObservableCollection<Certificate>();
                    foreach (Certificate certificate in Configuration.Instance.CertificateList)
                    {
                        if (certificate.CertificateType == _FilterCertificateType)
                            filteredCertificateList.Add(certificate);
                    }
                    return filteredCertificateList;
                }
                return Configuration.Instance.CertificateList; 
            }
        }   // End Property CertificateList

        public Certificate SelectedCertificate 
        { 
            get { return _SelectedCertificate; }
            set
            {
                _SelectedCertificate = value;
                NotifyOfPropertyChange(() => SelectedCertificate);
                NotifyOfPropertyChange(() => CanEditCertificate);
                NotifyOfPropertyChange(() => CanRemoveCertificate);
            }
        }   // End Property SelectedCertificate

        public bool CanEditCertificate
        {
            get { return _SelectedCertificate != null; }
        }   // End Property CanEditCertificate

        public bool CanRemoveCertificate 
        { 
            get { return _SelectedCertificate != null; }
        }   // End Property CanRemoveCertificate
        #endregion

        #region Methods
        public void DoubleClickCertificate()
        {
            if (_SelectMode)
            {
                _DialogWindow.DialogResult = true;
                _DialogWindow.Close();
            }
            else
                EditCertificate();
        }   // End Method DoubleClickCertificate

        public void AddCertificate()
        {
            CertificateViewModel certificateView;

            certificateView = new CertificateViewModel();
            if (WindowManagement.ShowDialog(certificateView))
                NotifyOfPropertyChange(() => CertificateList);
        }   // End Method AddCertificate

        public void EditCertificate()
        {
            CertificateViewModel certificateView;

            certificateView = new CertificateViewModel(_SelectedCertificate);
            if (WindowManagement.ShowDialog(certificateView))
                NotifyOfPropertyChange(() => CertificateList);

        }   // End Method EditCertificate

        public void RemoveCertificate()
        {
            if (SendEncryptedEmailMessageBox.Show("Are you sure you wish to remove the certificate?", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                return;

            Configuration.RemoveCertificate(_SelectedCertificate);
            NotifyOfPropertyChange(() => CertificateList);

        }   // End Method DeleteCertificate
        #endregion

        #region WindowStuff
        public void AttachView(object view, object context = null)
        {
            _DialogWindow = view as Window;
            if (_ViewAttached != null)
                _ViewAttached(this, new ViewAttachedEventArgs() { Context = context, View = view });
        }   // End Method AttachedView

        public override object GetView(object context = null)
        {
            return _DialogWindow;
        }   // End Method GetView

        protected override void OnInitialize()
        {
            if (_SelectMode)
                _DialogWindow.Title = "Select Certificate";
            else
                _DialogWindow.Title = "Certificate Management";
            _DialogWindow.Loaded += DialogWindowLoaded;

        }   // End Method OnInitialize

        private void DialogWindowLoaded(object sender, EventArgs e)
        {

            // Add code to be run after the window has been loaded

        }   // End Method DialogWindowLoaded
        #endregion

    }   // End Class CertificateManagementViewModel

}   // End Namespace SendEncryptedEmail.ViewModel
