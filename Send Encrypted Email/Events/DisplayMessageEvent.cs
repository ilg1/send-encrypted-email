﻿using System.Threading;
using System.Windows;

namespace SendEncryptedEmail.Events
{
    public class DisplayMessageEvent
    {
        public DisplayMessageEvent(string message, MessageBoxButton buttonStyle = MessageBoxButton.OK,
            MessageBoxImage imageStyle = MessageBoxImage.None, ManualResetEvent notifyComplete = null)
        {
            Message = message;
            ButtonStyle = buttonStyle;
            ImageStyle = imageStyle;
            NotifyComplete = notifyComplete;
        }

        public string Message { get; private set; }
        public MessageBoxButton ButtonStyle { get; private set; }
        public MessageBoxImage ImageStyle { get; private set; }
        public ManualResetEvent NotifyComplete { get; private set; }
        public MessageBoxResult Result { get; set; }

    }   // End Class DisplayMessageEvent

}   // End Namespace SendEncryptedEmail.Events
