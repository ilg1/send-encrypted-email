﻿using EASendMail;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SendEncryptedEmail.Control
{
    /// <summary>
    /// Interaction logic for EmailAttachmentBox.xaml
    /// </summary>
    public partial class EmailAttachmentBox : UserControl
    {
        public EmailAttachmentBox()
        {
            InitializeComponent();
        }   // End Constructor EmailAttachmentBox

        #region Properties
        public EmailAttachmentBox Instance
        {
            get { return this; }
        }   // End Property Instance
        #endregion

        #region Dependency Properties
        #region Contact List Dependency Property
        public ObservableCollection<Attachment> AttachmentList
        {
            get { return (ObservableCollection<Attachment>)GetValue(AttachmentListProperty); }
            set { SetValue(AttachmentListProperty, value); }
        }   // End Property AttachmentList

        public static readonly DependencyProperty AttachmentListProperty =
            DependencyProperty.Register("AttachmentList", typeof(ObservableCollection<Attachment>), typeof(EmailAttachmentBox), new PropertyMetadata(null));
        #endregion
        #endregion

        #region Methods
        public void RemoveAttachment(Attachment attachment)
        {
            if (attachment == null)
                return;

            AttachmentList.Remove(attachment);

        }   // End Method RemoveAttachment
        #endregion

    }   // End Class EmailAttachmentBox

}   // End Namespace SendEncryptedEmail.Control
