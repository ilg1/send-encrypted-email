﻿using SendEncryptedEmail.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SendEncryptedEmail.Control
{
    /// <summary>
    /// Interaction logic for EmailAddressBox.xaml
    /// </summary>
    public partial class EmailAddressBox : UserControl
    {
        #region Constructor
        public EmailAddressBox()
        {
            InitializeComponent();
        }   // End Constructor EmailAddressBox
        #endregion

        #region Properties
        public EmailAddressBox Instance 
        { 
            get { return this; }
        }   // End Property Instance
        #endregion

        #region Dependency Properties
        #region Contact List Dependency Property
        public ObservableCollection<Contact> ContactList
        {
            get { return (ObservableCollection<Contact>)GetValue(ContactListProperty); }
            set { SetValue(ContactListProperty, value); }
        }   // End Property ContactList

        public static readonly DependencyProperty ContactListProperty =
            DependencyProperty.Register("ContactList", typeof(ObservableCollection<Contact>), typeof(EmailAddressBox), new PropertyMetadata(null));
        #endregion
        #endregion

        #region Methods
        public void RemoveContact(Contact contact)
        {
            if (contact == null)
                return;

            ContactList.Remove(contact);

        }   // End Method RemoveContact
        #endregion
    }   // End Class EmailAddressBox

}   // End Namespace SendEncryptedEmail
